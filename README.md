# timeIO - the Timeseries Management System (TSM) of the Helmholtz Center for Environmental Research (UFZ)

[<img src="images/RDM_subline_fullcolor_rgb.png" alt="RDM team Logo" height="100px" />](https://www.ufz.de/index.php?en=45348)

[<img src="images/UFZ_Logo_RGB_EN_XS.png" alt="UFZ Logo" height="120px" />](https://www.ufz.de/)

**Welcome to the gitlab code repositories of timeIO.**

## A transferable infrastructure to enable FAIR timeseries data

The field of earth system sciences relies heavily on the collection, analysis, and interpretation of data to understand
complex spatiotemporal environment processes and predict future trends. Time series data, which captures measurements or
observations at regular intervals over time, plays a crucial role in elucidating patterns, detecting changes, and
informing decision-making in various environmental domains. However, the effective storage and management of time series
data present significant challenges that necessitate the development of robust and scalable data infrastructures.

A well-designed data infrastructure is crucial to ensure the reliability, accessibility, interoperability, and
sustainability of time series data across different domains and scales. It enables efficient data collection through
automated sensing technologies, standardized data exchange protocols, and quality control procedures. Moreover, robust
time series data infrastructures facilitate the integration of data from diverse sources into distributed data
infrastructures on national and/or continental scales and advances the dissemination of data to a wide range of
stakeholders including scientists, policymakers, resource managers, and the general public.

The focus lies on enhancing accessibility for end users, operators, system integrators, and maintainers. The time series
management system encompasses all essential components. It features a user-centric web-based frontend, a versatile data
integration layer, a robust time series database, efficient object storage, real-time quality control, and comprehensive
data visualization capabilities. It supports modern and classical data transfer protocols, and ensures compliance with
OGC standards for data access. Moreover, our fully integrated and containerized solution offers the convenience of swift
and effortless deployment within minutes and allows the seamless integration with existing services such as databases,
identity providers, and object storages.

This innovative infrastructure has the potential to significantly enhance environmental data management, fostering more
efficient research and facilitating informed decision-making processes.

## Requirements

Simply [FAIR](http://doi.org/10.1038/sdata.2016.18)

### Science- and Management friendly

Provide interoperable and reliable timeseries data enriched by metadata and with provenance information.

### User friendly

Easy to use user interface for people that produce or manage timeseries data streams, without requiring knowledge about
underlying technologies like databases. Review datastream content in realtime.

### Admin friendly

A scalable and transferable container based solution that will smoothly integrate into typical scientific IT landscapes.

### Developer friendly

Common open source solutions structured by microservice architecture to keep it open and simple to extend for
developers.

## Features

- File based raw data ingest with common protocols (FTP, SFTP, S3)
- Stream based raw data ingest with MQTT (TLS required)
- [HIFIS/Helmholtz AAI Login](https://hifis.net/doc/helmholtz-aai/) for end users - all people associated to
  institutions that are connected to [GÉANT Edugain](https://technical.edugain.org/entities) can log in from anywhere
- Grouping data and things by projects that are organized
  as [HIFIS/Helmholtz AAI Virtual Organisation (VO)](https://hifis.net/doc/helmholtz-aai/howto-vos/)
- Instant data visualisation of incoming data in Grafana: New Datastreams automatically get a dynamic dashboard with no
  need but the ability for further configuration and customisation
- Integration layer to link datastreams to sensor metadata in
  the [Sensor Management System (SMS)](https://helmholtz.software/software/hub-terra-sensor-management-system)
- Dynamic OGC STA endpoints with [SMS](https://helmholtz.software/software/hub-terra-sensor-management-system) metadata
  integration per project to access data in a standardized way

## License

All software and components written within the timeIO project is currently licensed under
the [HEESIL](https://git.gfz-potsdam.de/rse/heesil).

## Quickstart

1. Install a [`git`](https://git-scm.com/) client, [`Docker Engine`](https://docs.docker.com/engine/install/) (Community
   Edition - CE is enough) and [`Docker Compose`](https://docs.docker.com/compose/install/)
2. Checkout the [`tsm-orchestration`](https://codebase.helmholtz.cloud/ufz-tsm/tsm-orchestration) repository
3. Run `cp .env.example .env`
4. Run `docker copmose up` and give it some seconds to start up
5. Browse [http://localhost](http://localhost)

For extended configuration options follow the instructions in
the [README.md](https://codebase.helmholtz.cloud/ufz-tsm/tsm-orchestration/-/blob/main/README.md) of
the [`tsm-orchestration`](https://codebase.helmholtz.cloud/ufz-tsm/tsm-orchestration) repo.

## Techstack, dependencies and third party open source products

<img src="images/timeIO_techstack.png" width="500" alt="Techstack" />

<details open>
<summary>  
Full list of technologies (in alphabetical order)
</summary>  

| Technology | References |
|---|---|
| Alpine | [GitHub](https://github.com/alpinelinux/docker-alpine) \| [Website](https://alpinelinux.org/) |
| CAdvisor | [GitHub](https://github.com/google/cadvisor) |
| Click | [GitHub](https://github.com/pallets/click/) \| [Website](https://click.palletsprojects.com) |
| Debian | [DockerHub](https://hub.docker.com/_/debian) |
| Django | [GitHub](https://github.com/django/django) \| [Website](https://www.djangoproject.com/) |
| django-helmholtz-aai | [GitHub](https://codebase.helmholtz.cloud/hcdc/django/django-helmholtz-aai) |
| Docker CE | [GitHub](https://github.com/django/django) |
| Docker Compose | [GitHub](https://github.com/docker/compose) |
| FastAPI | [GitHub](https://github.com/tiangolo/fastapi) \| [Website](https://fastapi.tiangolo.com/) |
| fastavro | [GitHub](https://github.com/fastavro/fastavro) \| [Website](https://fastavro.readthedocs.io) |
| FROST | [GitHub](https://github.com/FraunhoferIOSB/FROST-Server) \| [Website](https://www.iosb.fraunhofer.de/en/projects-and-products/frost-server.html) |
| Grafana | [GitHub](https://github.com/grafana/grafana) \| [Website](https://grafana.com/) |
| MinIO | [GitHub](https://github.com/minio/minio) \| [Website](https://min.io/) |
| Mosquitto | [GitHub](https://github.com/eclipse/mosquitto) \| [Website](https://mosquitto.org/) |
| Nginx | [GitHub](https://github.com/nginx) \| [Website](https://www.nginx.com/) |
| NumPy | [GitHub](https://github.com/numpy/numpy) \| [Website](https://numpy.org/) |
| Paho MQTT | [GitHub](https://github.com/eclipse/paho.mqtt.python) \| [Website](https://eclipse.dev/paho/) |
| Pandas | [GitHub](https://github.com/pandas-dev/pandas) \| [Website](https://pandas.pydata.org/) |
| Postgres | [GitHub](https://github.com/postgres/postgres) \| [Website](https://www.postgresql.org/) |
| Psycopg 2 | [GitHub](https://github.com/psycopg/psycopg2) \| [Website](https://www.psycopg.org/) |
| Pydantic | [GitHub](https://github.com/pydantic/pydantic) \| [Website](https://pydantic.dev/) |
| PyYAML | [GitHub](https://github.com/yaml/pyyaml) |
| SaQC | [GitLab](https://git.ufz.de/rdm-software/saqc) \| [Website](https://rdm-software.pages.ufz.de/saqc/) |
| TimescaleDB | [GitHub](https://github.com/timescale/timescaledb) \| [Website](https://www.timescale.com/) |
| Tomcat | [GitHub](https://github.com/apache/tomcat) \| [Website](https://tomcat.apache.org/) |
| Uvicorn | [GitHub](https://github.com/encode/uvicorn) \| [Website](https://www.uvicorn.org/) |

</details>

## Contact

Feel free to contact [David Schäfer](https://www.ufz.de/index.php?en=38093) and [Martin Abbrent]() or
the [UFZ RDM team](https://www.ufz.de/index.php?en=45348) in general with every question you may have.

## Contributors

- Martin Abbrent (UFZ)
- David Schäfer (UFZ)
- Florian Gransee (UFZ)
- Joost Hemmen (UFZ)
- Tobias Kuhnert (UFZ)
- Luca Nendel (UFZ)
- Bert Palm (UFZ)
- Maximilian Schaldach (UFZ)
- Christian Schulz (UFZ)
- Martin Schrön (UFZ)
- Steffen Zacharias (UFZ)
- Thomas Schnicke (UFZ)
- Jan Bumberger (UFZ)

[Imprint](https://www.ufz.de/index.php?en=36683)

